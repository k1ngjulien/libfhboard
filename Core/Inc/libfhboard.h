#pragma once

/**
 *  _   _   _        __   _       _                                  _
 * | | (_) | |__    / _| | |__   | |__     ___     __ _   _ __    __| |
 * | | | | | '_ \  | |_  | '_ \  | '_ \   / _ \   / _` | | '__|  / _` |
 * | | | | | |_) | |  _| | | | | | |_) | | (_) | | (_| | | |    | (_| |
 * |_| |_| |_.__/  |_|   |_| |_| |_.__/   \___/   \__,_| |_|     \__,_|
 *
 * Library to drive 7-Segment-Displays and LEDs on the FH-Campus Wien STM32 Interface Board
 * Version 2.4
 *
 * README:
 * To use this library, *first* configure the pins connected to the 7-Segment-Displays as Outputs.
 * (See the pinout in white above the digits)
 *
 * Then make sure "displaySegments()" gets called every 1ms (I suggest putting it in stm32f4xx_it.c -> SysTick_Handler())
 *
 * From then on you can use the functions provided here to show numbers and symbols
 * on the 7-Segment-Displays and the library will handle the multiplexing for you.
 *
 * (c) 2022-2023: Julian Kandlhofer (https://jkandlhofer.com)
 * License: GPL3
 */

#include "stm32f4xx_hal.h"
#include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif
/**
 * Store Pin and Port of a GPIO together
 */
typedef struct
{
    GPIO_TypeDef* Port;
    uint16_t      Pin;
} GPIO_t;

/** Store a Symbol
 *
 * 7-Segment Pinout:
 *
 *    / (A) \
 *   (F)   (B)
 *      (G)
 *   (E)   (C)
 *    \ (D) 0
 *
 * 7-Segments + Decimal Point (8-Bit) are Stored as a Byte,
 * Symbol_t example = (Symbol_t)0b10000000
 *                                ^^^^^^^^
 *                                ABCDEFG.
 *
 * Symbol_t numberOne = SEG_B | SEG_C;
 * Symbol_t numberTwo = SEG_A | SEG_B | SEG_D | SEG_E | SEG_G;
 * ...
 */
typedef uint8_t Symbol_t;

#define SEG_NUM_DIGITS 4

#define SEG_A  ((Symbol_t)0b00000001)
#define SEG_B  ((Symbol_t)0b00000010)
#define SEG_C  ((Symbol_t)0b00000100)
#define SEG_D  ((Symbol_t)0b00001000)
#define SEG_E  ((Symbol_t)0b00010000)
#define SEG_F  ((Symbol_t)0b00100000)
#define SEG_G  ((Symbol_t)0b01000000)
#define SEG_DP ((Symbol_t)0b10000000)

// all internally available symbols (0-9 and A-F)
#define SEG_NUM_SYMBOLS 16
extern const Symbol_t SegSymbols[SEG_NUM_SYMBOLS];


// pins to drive the led bar
extern const GPIO_t LedPins[8];

/**
 * Multiplex the internally buffered symbols on the 4 7-Segment displays.
 *
 * to be called every 1ms (put me in stm42f4xx_it.c -> void SysTick_Handler())
 */
void displaySegments();

/**
 * Show a 4 digit decimal number on the 7-Segment displays.
 *
 * Will add leading zeroes.
 *
 * @param number number to show
 */
void segSetNumberDec(uint32_t number);

/**
 * Show two two-digit decimal numbers on the 7-Segment displays.
 *
 * @param left number to show on the left two segments
 * @param right number to show on the right two segments
 */
void segSetTwoNumbersDec(uint16_t left, uint16_t right);

/**
 * Show a signed 4 digit decimal number on the 7-Segment displays.
 *
 * If the number is negative, the first segment will show a minus,
 * therefore negative numbers can only be 3 digits long.
 *
 * @param number number to show
 */
void segSetSignedNumberDec(int32_t number);

/**
 * Show a 4 digit hexadecimal number on the 7-Segment displays.
 *
 * Will add leading zeroes.
 *
 * @param number number to show
 */
void segSetNumberHex(uint32_t number);

/**
 * Show two two-digit hexadecimal numbers on the 7-Segment displays.
 *
 * @param left number to show on the left two segments
 * @param right number to show on the right two segments
 */
void segSetTwoNumbersHex(uint16_t left, uint16_t right);

/**
 * Turn off all digit segments and decimal points.
 */
void segClearAll();

/**
 * Turn off all decimal points.
 */
void segClearDecimalPoints();

/**
 * Turn off all digit segments.
 */
void segClearDigits();

/**
 * Enable/Disable the decimal point on the selected segment.
 * Needs to be called *after* setting a number or symbol as it will be overridden otherwise
 *
 * @param digitNumber digit index (0-3) from left to right
 * @param decimalPoint enable or disable the decimal point
 */
void segSetDecimalPoint(uint8_t digitNumber, bool decimalPoint);

/**
 * Manually set a symbol for a specific segment.
 *
 * @param digitIndex (0-3)
 * @param symbol symbol to show
 */
void segSetSymbol(uint8_t digitIndex, Symbol_t symbol);

/**
 * Show a value on the LED-Bar.
 * 0 ... all off
 * 8 ... all on
 *
 * @param value Value to display (0 to 8)
 */
void setLedBar(uint8_t value);

/**
 * Set state of an output pin.
 *
 * @param pin port and pin to set
 * @param state set or reset
 */
void setPin(GPIO_t pin, GPIO_PinState state);

/**
 * Toggle an output pin.
 *
 * @param pin port and pin to set
 */
void togglePin(GPIO_t pin);

#ifdef __cplusplus
}
#endif
