/**
 * (c) Julian Kandlhofer, 2022-2023
 * License: GPL3
 */

#include "libfhboard.h"

// region GPIO definitions

const GPIO_t SegDigitEnablePins[SEG_NUM_DIGITS] = {
        {GPIOA, GPIO_PIN_7},
        {GPIOA, GPIO_PIN_6},
        {GPIOC, GPIO_PIN_5},
        {GPIOE, GPIO_PIN_3},
};


// segments a-g, dp
const GPIO_t SegLedPins[8] = {
        {GPIOC, GPIO_PIN_12},
        {GPIOD, GPIO_PIN_0},
        {GPIOA, GPIO_PIN_8},
        {GPIOC, GPIO_PIN_11},
        {GPIOC, GPIO_PIN_10},
        {GPIOD, GPIO_PIN_6},
        {GPIOD, GPIO_PIN_7},
        {GPIOC, GPIO_PIN_9},
};

// led bar pins
const GPIO_t LedPins[8] = {
        {GPIOB, GPIO_PIN_5},
        {GPIOE, GPIO_PIN_9},
        {GPIOE, GPIO_PIN_8},
        {GPIOB, GPIO_PIN_0},
        {GPIOE, GPIO_PIN_6},
        {GPIOB, GPIO_PIN_8},
        {GPIOB, GPIO_PIN_9},
        {GPIOB, GPIO_PIN_1}
};

// endregion

// region Symbol Definitions

const Symbol_t SYM_OFF = 0;
const Symbol_t SYM_MINUS = SEG_G;
const Symbol_t SYM_ZERO = SEG_A | SEG_B | SEG_C | SEG_D | SEG_E | SEG_F;
const Symbol_t SYM_ONE = SEG_B | SEG_C;
const Symbol_t SYM_TWO = SEG_A | SEG_B | SEG_D | SEG_E | SEG_G;
const Symbol_t SYM_THREE = SEG_A | SEG_B | SEG_C | SEG_D | SEG_G;
const Symbol_t SYM_FOUR = SEG_B | SEG_C | SEG_F | SEG_G;
const Symbol_t SYM_FIVE = SEG_A | SEG_C | SEG_D | SEG_F | SEG_G;
const Symbol_t SYM_SIX = SEG_A | SEG_C | SEG_D | SEG_E | SEG_F | SEG_G;
const Symbol_t SYM_SEVEN = SEG_A | SEG_B | SEG_C;
const Symbol_t SYM_EIGHT = SEG_A | SEG_B | SEG_C | SEG_D | SEG_E | SEG_F | SEG_G;
const Symbol_t SYM_NINE = SEG_A | SEG_B | SEG_C | SEG_D | SEG_F | SEG_G;
const Symbol_t SYM_A = SEG_A | SEG_B | SEG_C | SEG_E | SEG_F | SEG_G;
const Symbol_t SYM_B = SEG_C | SEG_D | SEG_E | SEG_F | SEG_G;
const Symbol_t SYM_C = SEG_A | SEG_D | SEG_E | SEG_F;
const Symbol_t SYM_D = SEG_B | SEG_C | SEG_D | SEG_E | SEG_G;
const Symbol_t SYM_E = SEG_A | SEG_D | SEG_E | SEG_F | SEG_G;
const Symbol_t SYM_F = SEG_A | SEG_E | SEG_F | SEG_G;

const Symbol_t SegSymbols[SEG_NUM_SYMBOLS] = {
        SYM_ZERO,
        SYM_ONE,
        SYM_TWO,
        SYM_THREE,
        SYM_FOUR,
        SYM_FIVE,
        SYM_SIX,
        SYM_SEVEN,
        SYM_EIGHT,
        SYM_NINE,
        SYM_A,
        SYM_B,
        SYM_C,
        SYM_D,
        SYM_E,
        SYM_F
};

// endregion

// region Library Internals

// internal segment state
// is copied before each segment gets displayed
static Symbol_t NextSegmentState[SEG_NUM_DIGITS] = {0};

static void segDisableAll() {
  for (int i = 0; i < 8; ++i) {
    setPin(SegLedPins[i], GPIO_PIN_RESET);
  }
}

static void segShowSymbol(Symbol_t symbol) {
  for (int i = 0; i < 8; ++i) {
    bool isOn = (symbol & (1 << i)) != 0;
    GPIO_PinState state = isOn ? GPIO_PIN_SET : GPIO_PIN_RESET;
    setPin(SegLedPins[i], state);
  }
}

static void segSelectSegment(uint8_t digitIndex) {
  for (int i = 0; i < SEG_NUM_DIGITS; ++i) {
    GPIO_PinState state = i == digitIndex ? GPIO_PIN_RESET : GPIO_PIN_SET;
    setPin(SegDigitEnablePins[i], state);
  }
}

// endregion

// region Exposed Library Functions

const int defaultShowDelay = 5; // [ms]
// to be called in 1ms hal systick interrupt
// multiplexes between the led segments
void displaySegments() {
  static Symbol_t currentSymbol = SYM_OFF;
  static int showDelay = defaultShowDelay;
  static int currentSegmentIndex = 0;

  segSelectSegment(currentSegmentIndex);
  segShowSymbol(currentSymbol);

  // show currentSymbol segment for a set time
  if (--showDelay != 0)
    return;
  showDelay = defaultShowDelay;

  // turn off all segment leds before switching currentSymbol and segment
  // prevents ghost image of previous currentSegmentIndex showing up by
  // being off for 1ms until the next systick
  segDisableAll();
  currentSegmentIndex = (currentSegmentIndex + 1) % SEG_NUM_DIGITS;
  currentSymbol = NextSegmentState[currentSegmentIndex];
}

void segSetSymbol(uint8_t digitIndex, Symbol_t symbol) {
  NextSegmentState[digitIndex] = symbol;
}

void segSetTwoNumbersDec(uint16_t left, uint16_t right) {
  segSetNumberDec(100 * left + right);
}

void segSetTwoNumbersHex(uint16_t left, uint16_t right) {
  segSetNumberHex(256 * left + right);
}

void segSetNumberDec(uint32_t number) {
  for (int i = 3; i >= 0; --i) {
    NextSegmentState[i] = SegSymbols[number % 10];
    number /= 10;
  }
}

void segSetSignedNumberDec(int32_t number) {
  // positive number
  if (number >= 0) {
    segSetNumberDec(number);
    return;
  }

  // negative number
  number = -number;
  segSetNumberDec(number);
  NextSegmentState[0] = SYM_MINUS;
}

void segSetNumberHex(uint32_t number) {
  for (int i = 3; i >= 0; --i) {
    NextSegmentState[i] = SegSymbols[number % 16];
    number /= 16;
  }
}

void segClearAll() {
  for (int i = 0; i < SEG_NUM_DIGITS; ++i) {
    NextSegmentState[i] = SYM_OFF;
  }
}

void segClearDigits() {
  for (int i = 0; i < SEG_NUM_DIGITS; ++i) {
    NextSegmentState[i] &= SEG_DP;
  }
}

void segClearDecimalPoints() {
  for (int i = 0; i < SEG_NUM_DIGITS; ++i) {
    NextSegmentState[i] &= ~SEG_DP;
  }
}

void segSetDecimalPoint(uint8_t digitNumber, bool decimalPoint) {
  // set or reset decimal point bit
  if (decimalPoint) {
    NextSegmentState[digitNumber] |= SEG_DP;
  } else {
    NextSegmentState[digitNumber] &= ~SEG_DP;
  }
}

void setPin(GPIO_t pin, GPIO_PinState state) {
  HAL_GPIO_WritePin(pin.Port, pin.Pin, state);
}

void togglePin(GPIO_t pin) {
  HAL_GPIO_TogglePin(pin.Port, pin.Pin);
}

void setLedBar(uint8_t value) {
  for (int i = 0; i < 8; ++i) {
    GPIO_PinState state = i < value ? GPIO_PIN_SET : GPIO_PIN_RESET;
    setPin(LedPins[i], state);
  }
}

// endregion
