```
 _   _   _        __   _       _                                  _
| | (_) | |__    / _| | |__   | |__     ___     __ _   _ __    __| |
| | | | | '_ \  | |_  | '_ \  | '_ \   / _ \   / _` | | '__|  / _` |
| | | | | |_) | |  _| | | | | | |_) | | (_) | | (_| | | |    | (_| |
|_| |_| |_.__/  |_|   |_| |_| |_.__/   \___/   \__,_| |_|     \__,_|
```
 

Library to drive 7-Segment-Displays and LEDs on the FH-Campus Wien STM32 Interface Board
Version 2.4

### README:
To use this library, *first* configure the pins connected to the 7-Segment-Displays as Outputs.
(See the pinout in white above the digits)

Then make sure "displaySegments()" gets called every 1ms (I suggest putting it in stm32f4xx_it.c -> SysTick_Handler())

From then on you can use the functions provided here to show numbers and symbols
on the 7-Segment-Displays and the library will handle the multiplexing for you.

![full fh baseboard](doc/img/full_board.jpg)


![extension board with 7-Segments, LEDs, Potis, Buttons and Switches](doc/img/led_board.jpg)

![7-Segement-Display showing symbols 0-9 A-F](doc/img/VID-20221120-WA0006.mp4)

(c) 2022-2023: Julian Kandlhofer (https://jkandlhofer.com)
License: GPL3
